<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<!-- Favicon Icon -->
<link rel="icon" href="assets/img/favicon.ico" />
<title>Sensor Cloud Register Virtual Network</title>
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<style>
body { //
	background: url(http://mymaplist.com/img/parallax/back.png);
	background-color: #444;
}

.node {
	cursor: pointer;
}

.node circle {
	fill: #fff;
	stroke: steelblue;
	stroke-width: 3px;
}

.node text {
	font: 12px sans-serif;
}

.link {
	fill: none;
	stroke: #ccc;
	stroke-width: 2px;
}
</style>
<!-- BOOTSTRAP CORE CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<!-- ION ICONS STYLES -->
<link href="assets/css/ionicons.css" rel="stylesheet" />
<!-- FONT AWESOME ICONS STYLES -->
<link href="assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM CSS -->
<link href="assets/css/style.css" rel="stylesheet" />
<!-- IE10 viewport hack  -->
<script src="assets/js/ie-10-view-port.js"></script>
<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
	<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo-wrapper">
				<img src="assets/img/logo.png" alt="Sensor Cloud" />
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
				<div class="menu-links scroll-me">
					<a href="Loginservlet"> <i class="ion-ios-home-outline"></i>
					</a> <a href=VirtualNodeServlet>Create VNode</a>
				</div>
			</div>
		</div>
	</div>
	</header>
	<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
	<!-- This is a very simple parallax effect achieved by simple CSS 3 multiple backgrounds, made by http://twitter.com/msurguy -->



	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Registration status</h3>
			</div>

			<div id="responseDiv" style="display: none;"
				panel-body" style="color: black; font-weight: bold; font-style: italic;"
				align="center"></div>
			<div id="errorDiv" style="display: none;" color: red; font-weight:
				bold; font-style: italic;" align="center"></div>
		</div>
	</div>
	<div>
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Register your network</h3>
					</div>
					<div class="panel-body">
						<form accept-charset="UTF-8" role="form">
							<fieldset>
								<!-- <div id="errorDiv"
									style="color: red; font-weight: bold; font-style: italic;"
									align="center"></div> -->
								<div class="form-group">
									<input id="nName" class="form-control"
										placeholder="Virtual Network Name" name="nName" type="text">
								</div>
								<input id="btnRegisterVnetwork" class="btn btn-lg btn-success btn-block"
									type="submit" value="Create Virtual Network">
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$("#btnRegisterVnetwork")
									.click(
											
											function(e) {
												$("#errorDiv")
												.html("");
												e.preventDefault();
												var value1 = $("#nName")
														.val();
												if (value1 != null && value1 != "") {
													var data = {};
													data.networkName = $("#nName")
															.val();
													data.networkID = value1.replace(/ /g,"_");
													console.log(localStorage['userAuth']);
													$
															.ajax({
																url : 'http://52.8.17.20:80/registerVNetwork',
																type : 'PUT',
																contentType : 'application/json; charset=utf-8',
																headers: {"Authorization": localStorage['userAuth']},
																dataType : 'json',
																data : JSON
																		.stringify(data),
																success : function(
																		data) {
																	$("#errorDiv")
																	.html("Network registration success!");
																	alert("success");
// 																	document.getElementById('errorDiv').style.display = "block";
// 																	document.getElementById('responseDiv').style.display = "block";
// 																	$('#responseDiv').html('NetworkID: ' + data.NetworkID.S + '<br />NetworkName: ' + data.NetworkName.S + '<br />NetworkName: ' + data.RegionID.S + '<br />Owner: ' + data.Owner.S	);
																	
																	console
																			.log('Network registration success!');
																	console
																			.log(JSON
																					.stringify(data));
																},
																
																error : function(
																		jqXHR,
																		textStatus) {
																	$(
																			"#errorDiv")
																			.html(
																					"Network name exists. please provide a unique name.")
																	console
																			.log('error::'
																					+ data);

																}
															});
												} else {
													$("#errorDiv")
															.html(
																	"Provide proper input")
												}

											});
		
							$(document)
									.mousemove(
											function(event) {
												TweenLite
														.to(
																$('body'),
																.5,
																{
																	css : {
																		backgroundPosition : ""
																				+ parseInt(event.pageX / 8)
																				+ "px "
																				+ parseInt(event.pageY
																						/ '12')
																				+ "px, "
																				+ parseInt(event.pageX
																						/ '15')
																				+ "px "
																				+ parseInt(event.pageY
																						/ '15')
																				+ "px, "
																				+ parseInt(event.pageX
																						/ '30')
																				+ "px "
																				+ parseInt(event.pageY
																						/ '30')
																				+ "px"
																	}
																});
											}); 
						});
		
	</script>
</body>
</html>