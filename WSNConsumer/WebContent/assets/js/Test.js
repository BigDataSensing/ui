/**
 * hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
 * <http://cherne.net/brian/resources/jquery.hoverIntent.html>
 * 
 * @param  f  onMouseOver function || An object with configuration options
 * @param  g  onMouseOut function  || Nothing (use configuration options object)
 * @author    Brian Cherne brian(at)cherne(dot)net
 */
(function($){$.fn.hoverIntent=function(f,g){
	var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);





























$=jQuery.noConflict();
$(document).ready(function(){
	divSectionImages = $('.divSectionImage');
	var sectionConfig = {    
			over: function() {  // function = onMouseOver callback (REQUIRED) 

				var divSections = $('#feature-sections .divSectionImage');
				console.log($(this).innerHTML+ "over");
				$.each(divSections, function(index, divSectionImage){

					if($(divSectionImage).hasClass('hover')){
						$(divSectionImage).removeClass('hover');
						onFocusOutAnimate($(divSectionImage));

					} 

				});
				if($(this).hasClass('selected')){
					if($(this).width()==80){
						$(this).addClass("hover");
						onFocusAnimate($(this));
					}
				} else {
					var selectedSection = $('#feature-sections .selected');
					onFocusOutAnimate($(selectedSection));
					$(this).addClass("hover");
					onFocusAnimate($(this));
				}                                         
			}, 
			timeout: 0, // number = milliseconds delay before onMouseOut
			sensitivity:10,
			out: function() { 
			}  
	};

	//onmouseover-out of the categoryStrip
	divSectionImages.hoverIntent(sectionConfig);

	var featureConfig = {    

			over: function(){

			},   
			sensitivity:10,
			out: function(){

				//hover out
				var divSections = $('#feature-sections .divSectionImage');
				$.each(divSections, function(index, divSectionImage){

					if($(divSectionImage).hasClass('hover')){

						onFocusOutAnimate($(divSectionImage));
						$(divSectionImage).removeClass('hover');

					}  

					else if($(divSectionImage).hasClass('selected')){
						onFocusAnimate($(divSectionImage));
					}

				});

			} 

	};



	//On mouseover-out of the complete feature category feature-sections

	$('#feature-sections').hoverIntent(featureConfig);                    





	divSectionImages.click(function(){
		var selectedSection = $('#feature-sections .selected');
		selectedSection.removeClass('selected');                
		$(this).addClass('selected');
		if($(this).hasClass('hover')){
			$(this).removeClass('hover');
		}
		console.log($(this).innerHTML+ "click");
	});                    
	/*
$(divSectionImages[1]).addClass('selected');
$(divSectionImages[1]).animate({
                            height: "70",
                            width : "100",
                            marginTop: "0"
                        }, 300); */

	$(divSectionImages[1]).trigger('click');  
});

onFocusAnimate = function (divSectionImage, shift){

	imgSection = divSectionImage.children('img');
	divSectionImage.animate({
		height: "90",
		width : "100",
		marginTop: "0"
	}, 300);
	imgSection.animate({
		height: "60",
		width : "100"

	}, 300);


};

onFocusOutAnimate = function (divSectionImage, shift){

	imgSection = divSectionImage.children('img');
	divSectionImage.animate({
		height: "80",
		width : "80",
		marginTop: "10"
	}, 300);
	imgSection.animate({
		height: "50",
		width : "80"

	}, 300);

};
